dnl AC_CHECK_LIBBASH
dnl Checks whether ldbash exists and then checks whether
dnl required libbash libraries are installed.
dnl If either ldbash or required libraries are missing - throws AC_MSG_ERROR.
dnl Example: AC_CHECK_LIBBASH([getopts hashstash])
AC_DEFUN([AC_CHECK_LIBBASH],[
	AC_PATH_PROG([LDBASH], [ldbash])
	[[ "X$LDBASH" == "X" ]] && AC_MSG_ERROR([libbash is not found!])
	AC_FOREACH([BASH_LIB],[$1],[
		AC_MSG_CHECKING([whether `m4_defn([BASH_LIB])' libbash library is listed]) && \
						 $LDBASH -l | grep -q m4_defn([BASH_LIB]) && \
						 echo yes || \
						 AC_MSG_ERROR([m4_defn([BASH_LIB]) not found!])
	])
])
