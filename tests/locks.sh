#!/bin/bash

###########################################################################
# Copyright (c) 2004-2009 Hai Zaar and Gil Ran                            #
#                                                                         #
# This program is free software; you can redistribute it and/or modify it #
# under the terms of version 3 of the GNU General Public License as       #
# published by the Free Software Foundation.                              #
#                                                                         #
###########################################################################

#
# Test suite for libbash locks library
#

# First of all, install libbash in an install directory

SOURCE="source ../lib/locks.sh; source ../lib/getopts.sh; source ../lib/messages.sh; source ../lib/hashstash.sh; source ../lib/colors.sh"
eval $SOURCE

LOCKS_DIR=/tmp/.dirlocks-$USER

[[ ${__locks_DEBUG_MODE} ]] && echo "MYPID=$$"

exit_status=0

echo "Sequence 1: dirInitLock"
	echo -en "\tTEST 1: Initialize a lock - no spin"
		mkdir -p /tmp/MyLock && \
		dirInitLock /tmp/MyLock && \
		[[ $(cat $LOCKS_DIR/tmp/MyLock/$$.spin) == "${__locks_DEFAULT_SLEEP_TIME}"  ]] && \
		printOK	|| (printFAIL && exit_status=1)
		
	echo -en "\tTEST 2: Initialize it again with spin 5"
		dirInitLock /tmp/MyLock 5 && \
		[[ $(cat $LOCKS_DIR/tmp/MyLock/$$.spin) == "5"  ]] && \
		printOK	|| (printFAIL && exit_status=1)
			
	echo -en "\tTEST 3: Initialize a second lock"
		SOURCE=$SOURCE \
		bash -c '
			eval $SOURCE || exit 1
			dirInitLock /tmp/MyLock && \
			printOK	|| (printFAIL && exit_status=1)
		'
			
	echo -en "\tTEST 4: Initialize a lock on a sub-directory"
		mkdir -p /tmp/MyLock/SubLock && \
		dirInitLock /tmp/MyLock/SubLock && \
		printOK	|| (printFAIL && exit_status=1)

	echo -en "\tTEST 5: Initialize a lock on a parent-directory"
		mkdir -p /tmp && \
		dirInitLock /tmp && \
		printOK	|| (printFAIL && exit_status=1)

	echo -en "\tTEST 7: Initialize a lock on a non-existing dir"
		non_existing="/non_existing"
		while [[ -e $non_existing ]] ; do
			non_existing=${non_existing}${non_existing}
		done
		dirInitLock $non_existing && \
		printOK	|| (printFAIL && exit_status=1)

	echo -en "\tTEST 8: Initialize a non-existing dir with illegal name"
		non_existing="/\.\./../\\\\\/non_existing/"
		while [[ -e $non_existing ]] ; do
			non_existing=${non_existing}${non_existing}
		done
		echo $non_existing
		! dirInitLock $non_existing && \
		printOK	|| (printFAIL && exit_status=1)
		
		
	echo -en "\tTEST 8: Initialize a lock on an 'object'"
		dirInitLock MyObj && \
		printOK	|| (printFAIL && exit_status=1)

	echo -en "\tTEST 9: Initialize a lock on a file"
		touch /tmp/MyLock/MyFile
		dirInitLock /tmp/MyLock/MyFile && \
		printOK	|| (printFAIL && exit_status=1)


echo "Sequence 2: dirTryLock"
	echo -en "\tTEST 1: Try lock an uninitialized lock - unlocked"
		dirTryLock NoLock
		[[ $? == ${__locks_ERR_NO_SPIN_FILE} ]] &&
		printOK	|| (printFAIL && exit_status=1)

	echo -en "\tTEST 2: Try lock an initialized lock - unlocked"
		dirTryLock MyObj 
		printOK	|| (printFAIL && exit_status=1)
		
	echo -en "\tTEST 3: Try lock an uninitialized lock - locked"
		SOURCE=$SOURCE \
		bash -c '
			eval $SOURCE || exit 1
			dirTryLock MyObj
			[[ $? == ${__locks_ERR_NO_SPIN_FILE} ]]
		' && \
		printOK	|| (printFAIL && exit_status=1)

	echo -en "\tTEST 4: Try lock an initialized lock - locked by me"
		dirTryLock MyObj 
		[[ $? == ${__locks_ERR_DIR_IS_LOCKED} ]] &&
		printOK	|| (printFAIL && exit_status=1)
	
	echo -en "\tTEST 5: Try lock an initialized lock - locked by someone else"
		SOURCE=$SOURCE \
		bash -c '
			eval $SOURCE || exit 1
			dirInitLock MyObj &&
			dirTryLock MyObj
			[[ $? == ${__locks_ERR_DIR_IS_LOCKED} ]]
		' && \
		printOK	|| (printFAIL && exit_status=1)
			
	# PREPARING FOR TESTS 6 AND 7 - Creating a leftover lock
	SOURCE=$SOURCE LOCKS_DIR=$LOCKS_DIR \
	bash -c '	
		eval $SOURCE || exit 1
		dirInitLock HisObj &&
		ls $LOCKS_DIR/HisObj > /dev/null 2>&1 && 
		dirTryLock HisObj
	'
	if [[ $? != 0 ]] ; then
		echo -e "\tPreparations for tests 6 and 7 failed! Skipping!"
		printATTN
	else
		echo -en "\tTEST 6: Try lock an uninitialized lock - there's a leftover lock"
			dirTryLock HisObj 
			[[ $? == ${__locks_ERR_NO_SPIN_FILE} ]] &&
			printOK	|| (printFAIL && exit_status=1)

		echo -en "\tTEST 7: Try lock an initialized lock - there's a leftover lock"
			dirInitLock HisObj && \
			dirTryLock HisObj && \
			printOK	|| (printFAIL && exit_status=1)
	fi

	echo -en "\tTEST 8: Try lock an initialized file lock - unlocked"
			dirTryLock /tmp/MyLock/MyFile && \
			printOK	|| (printFAIL && exit_status=1)

echo "Sequence 3: dirLock"
	echo -en "\tTEST 1: Lock an uninitialized lock - unlocked"
		dirLock NoLock
		[[ $? == ${__locks_ERR_NO_SPIN_FILE} ]] &&
		printOK	|| (printFAIL && exit_status=1)

	# PREPARING FOR TEST 2 - removing the lock
	dirUnlock MyObj
	if [[ $? != 0 ]] ; then
		echo -e "\tPreparations for test 2 failed! Skipping!"
		printATTN
	else
		echo -en "\tTEST 2: Lock an initialized lock - unlocked"
			dirLock MyObj &&
			printOK	|| (printFAIL && exit_status=1)
	fi
		
	echo -en "\tTEST 3: Lock an uninitialized lock - locked"
		SOURCE=$SOURCE \
		bash -c '
			eval $SOURCE || exit 1
			dirLock MyObj
			[[ $? == ${__locks_ERR_NO_SPIN_FILE} ]]
		' && \
		printOK	|| (printFAIL && exit_status=1)

	echo -en "\tTEST 4: Lock an initialized lock - locked by someone else"
		SOURCE=$SOURCE \
		bash -c '
			eval $SOURCE || exit 1
			dirInitLock MyObj
			dirLock MyObj
			' &
		dirLockPID=$!
	echo -en " - (sleeping for 10 seconds)"
		sleep 10
		exec 9>&2 2>/dev/null
		kill $dirLockPID > /dev/null 2>&1
		status=$?
		tput sgr0
		exec 2>&9
		[[ $status == 0 ]] && \
		printOK	|| (printFAIL && exit_status=1)
	
	# PREPARING FOR TEST 5 - removing the lock
	dirUnlock MyObj
	if [[ $? != 0 ]] ; then
		echo -e "\tPreparations for test 5 failed! Skipping!"
		printATTN
	else
		echo -en "\tTEST 5: Lock an initialized lock - locked by me"
			SOURCE=$SOURCE \
			bash -c '
				eval $SOURCE || exit 1
				dirInitLock MyObj
				dirTryLock MyObj || exit 1
				dirLock MyObj
				' &
			dirLockPID=$!
		echo -en " - (sleeping for 10 seconds)"
			sleep 10
			exec 9>&2 2>/dev/null
			kill $dirLockPID > /dev/null 2>&1
			status=$?
			tput sgr0
			exec 2>&9
			[[ $status == 0 ]] && \
			printOK	|| (printFAIL && exit_status=1)
	fi

dirTryLock MyObj
if [[ $? != 0 ]] ; then
	echo -e "\tPreparations for sequence 4 failed! Skipping!"
	printATTN
else
	echo "Sequence 4: dirUnlock"
		echo -en "\tTEST 1: Unlock an uninitialized lock - unlocked"
			dirUnlock NoLock
			[[ $? == ${__locks_ERR_NO_SPIN_FILE} ]] && \
			printOK	|| (printFAIL && exit_status=1)
		
		echo -en "\tTEST 2: Unlock an uninitialized lock - locked"
			SOURCE=$SOURCE \
			bash -c '
				eval $SOURCE || exit 1
				dirUnlock MyObj
			'
			[[ $? == ${__locks_ERR_NO_SPIN_FILE} ]] && \
			printOK	|| (printFAIL && exit_status=1)
		
		echo -en "\tTEST 3: Unlock an initialized lock - locked by someone else"
			SOURCE=$SOURCE \
			bash -c '
				eval $SOURCE || exit 1
				dirInitLock MyObj || exit 1
				dirUnlock MyObj
			'
			[[ $? == ${__locks_ERR_DIR_IS_LOCKED} ]] && \
			printOK	|| (printFAIL && exit_status=1)
		
		echo -en "\tTEST 4: Unlock an initialized lock - locked by me"
			dirUnlock MyObj && \
			printOK	|| (printFAIL && exit_status=1)
fi


echo "Sequence 4: dirDestroyLock"
	echo -en "\tTEST 1: Destroy an uninitialized lock - unlocked"
		SOURCE=$SOURCE \
		bash -c '
			eval $SOURCE || exit 1
			dirDestroyLock NoLock
		' && \
		printOK	|| (printFAIL && exit_status=1)
	
	echo -en "\tTEST 2: Destroy an uninitialized lock - locked"
		SOURCE=$SOURCE \
		bash -c '
			eval $SOURCE || exit 1
			dirDestroyLock MyObj
		' && \
		printOK	|| (printFAIL && exit_status=1)

	echo -en "\tTEST 3: Destroy an initialized lock - unlocked"
		SOURCE=$SOURCE \
		bash -c '
			eval $SOURCE || exit 1
			dirInitLock NoLock
			dirDestroyLock NoLock
		' && \
		printOK	|| (printFAIL && exit_status=1)
	
	echo -en "\tTEST 4: Destroy an initialized lock - locked by me"
		dirInitLock MyObj && \
		dirDestroyLock MyObj && \
		printOK	|| (printFAIL && exit_status=1)

	echo -en "\tTEST 5: Destroy an initialized lock - locked by someone else"
		SOURCE=$SOURCE \
		bash -c '
			eval $SOURCE || exit 1
			dirInitLock MyObj
			dirDestroyLock MyObj
		' && \
		printOK	|| (printFAIL && exit_status=1)

echo "Sequence 5: Preform final cleanup"
[[ ${__locks_DEBUG_MODE} ]] && echo "MYPID=$$"
	dirInitLock HisObj		|| (echo "FAILED: dirInitLock HisObj" && exit_status=1)
	dirInitLock MyObj		|| (echo "FAILED: dirInitLock MyObj" && exit_status=1)
	dirInitLock NoLock		|| (echo "FAILED: dirInitLock NoLock" && exit_status=1)
	dirUnlock HisObj		|| (echo "FAILED: dirUnlock HisObj" && exit_status=1)
	dirUnlock MyObj			|| (echo "FAILED: dirUnlock MyObj" && exit_status=1)
	dirUnlock NoLock		|| (echo "FAILED: dirUnlock NoLock" && exit_status=1)
	dirDestroyLock HisObj	|| (echo "FAILED: dirDestroyLock HisObj" && exit_status=1)
	dirDestroyLock MyObj	|| (echo "FAILED: dirDestroyLock MyObj" && exit_status=1)
	dirDestroyLock NoLock	|| (echo "FAILED: dirDestroyLock NoLock" && exit_status=1)

exit $exit_status
