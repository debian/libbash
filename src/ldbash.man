.\" Process this file with
.\" groff -mdoc -Tascii <filename>
.\"
.Dd July 28, 2009
.Os Linux
.Dt LDBASH \&1 "libbash Manual"
.\"
.Sh NAME
.\"
.\"
.Nm ldbash
.Nd Dynamic loader for 
.Xr libbash 7 libraries.
.\"
.\"
.Sh SYNOPSIS
.\"
.\"
.Nm 
.Op Fl h | Fl -help
.Nm 
.Op Fl l | Fl -list
.Nm 
.Op Fl L | Fl -load Ar lib,[lib] ...
.Nm 
.Op Fl U | Fl -unload Ar lib,[lib] ...
.Nm 
.Op Fl e | Fl -externlist Ar lib,[lib] ...
.Nm 
.Op Fl -externlist-all
.Nm 
.Op Fl -i | Fl -internlista Ar lib,[lib] ...
.Nm 
.Op Fl -internlist-all
.\"
.\"
.Sh DESCRIPTION
.\"
.\"
.Bd -filled
.Nm
is used to manipulate 
.Xr libbash 7
libraries. Its main function is to load specific library.
It can also print list of available libraries, list functions
each library exports, unload functions, etc.
.Pp
In case of 
.Fl -load
and
.Fl -unload
,the output is intended to be passed to bash 
.Em eval
command.
.Ed
.\"
.\"
.Sh Options
.\"
.\"
.Bd -filled
.Bl -tag -width 123456789123
.\"
.It Fl h | Fl -help
Print options summary
.\"
.It Fl l | Fl -list
Display list of available libraries. The libraries names listed, may be passed
as parameters to other invocations of 
.Nm .
I.e. first you run 
.Nm Fl -list
to see what is available and then you may load it.
.\"
.It Fl L | Fl -load Ar lib,[lib] ...
Load given libraries - i.e. print string that should be passed to 
.Em eval
command.  Usually the string contains various 
.Em source
commands.
.Pp
Libraries that given libraries depend on are also loaded.
.Pp
Libraries only loaded if their dependencies are satisfied. Dependencies are resolved
using 
.Pa ldbash.cache
file, which is created by 
.Xr ldbashconfig 8 .
.\"
.It Fl U | Fl -unload Ar lib,[lib] ...
Unload given libraries, but 
.Em not
their dependencies.
.Pp
The output should be passed to 
.Em eval
command (in the same manner as with
.Fl -load
).
.\"
.It Fl e | Fl -externlist Ar lib,[lib] ...
List all symbols that are exported by given libraries. Symbols are usually functions
that given libraries implement.
.\"
.It Fl -externlist-all
List all exported symbols of all available libraries. Symbols are usually functions
that library implement.
.\"
.It Fl i | Fl -internlist Ar lib,[lib] ...
List all internal symbols (i.e. global variables/functions) of given libraries.
This is intended mostly for developers that want to develop library of their own, and
want to be sure that they don't override any already defined symbol.
.\"
.It Fl -internlist-all
List all internal symbols of all available libraries.
This is intended mostly for developers that want to develop library of their own, and
want to be sure that they don't override any already defined symbol.
.\"
.El
.Ed
.\"
.\"
.Sh FILES
.\"
.\"
.Bd -filled
.Bl -tag -width 12345
.It Pa /etc/ldbash.cache
Cache file that contains information about libraries dependencies and list of exported
symbols. See
.Xr ldbashconfig (8)
for further details.
.El
.Ed
.\"
.\"
.Sh BUGS
.\"
.\"
.Bd -filled
The script can't load libraries if their file name starts with
.Ql - .
(If someone uses file names that start with a 
.Ql -
he/she deserves it!)
.Ed
.\"
.\"
.Sh AUTHORS
.\"
.\"
.An "Hai Zaar" Aq haizaar@gmail.com
.An "Gil Ran" Aq gil@ran4.net
.\"
.\"
.Sh SEE ALSO
.\"
.\"
.Xr libbash 7 ,
.Xr ldbashconfig 8
