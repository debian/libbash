.\" Process this file with
.\" groff -mdoc -Tascii <filename>
.\"
.Dd July 28, 2009
.Os Linux
.Dt LDBASHCONFIG \&8 "libbash Manual"
.\"
.\"
.Sh NAME
.\"
.\"
.Nm ldbashconfig 
.Nd Creates or updates cache file for 
.Xr libbash 7 libraries.
.\"
.\"
.Sh SYNOPSIS
.\"
.\"
.Nm
.\"
.\"
.Sh DESCRIPTION
.\"
.\"
.Bd -filled
.Nm 
Creates and/or updates 
.Xr libbash 7
cache file. This file is used by
.Xr ldbash 1
at run time.
.Pp
Run this command each time you add new bash library or change interface of existing one \-
.Xr ldbash 1
highly depends on consistency of the cache file.
.Ed
.\"
.\"
.Sh FILES
.\"
.\"
.Bd -filled
.Bl -tag -width 12345
.It Pa /etc/ldbash.cache
Cache file that contains information about libraries dependencies and list of exported
symbols. See
.Xr ldbashconfig (8)
for further details.
.El
.Ed
.\"
.\"
.Sh BUGS
.\"
.\"
None known.
.\"
.\"
.Sh AUTHORS
.An "Hai Zaar" Aq haizaar@gmail.com
.An "Gil Ran" Aq gil@ran4.net
.\"
.\"
.\"
.\"
.Sh "SEE ALSO"
.\"
.\"
.Xr ldbash 1 ,
.Xr libbash 7
