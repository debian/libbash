.\" Process this file with
.\" groff -mdoc -Tascii <filename>
.\"
.Dd July 28, 2009
.Os Linux
.Dt LOCKS \&3 "libbash locks Library Manual"
.\"
.Sh NAME
.\"
.\"
.Nm locks
.Nd libbash library that implements locking (directory based).
.Pp
.Sy This library is not throughoutly tested - use with caution!
.\"
.\"
.Sh SYNOPSIS
.\"
.Bl -tag -compact -width 123456789012345
.\"
.It Cm dirInitLock
.Aq object
.Op Aq spin
.\"
.It Cm dirTryLock
.Aq object
.\"
.It Cm dirLock
.Aq object
.\"
.It Cm dirUnlock
.Aq object
.\"
.It Cm dirDestroyLock
.Aq object
.\"
.\"
.\"
.El
.\"
.\"
.Sh DESCRIPTION
.\"
.\" GENERAL
.Ss General
.Nm
is a collection of functions that implement locking (mutex like) in bash scripting language.
The whole idea is based on the fact that directory creation/removal is an atomic process.
The creation of this library was inspired by studying CVS locks management.
.Pp
Same lock object can by used by several processes to serialize access to some shared resource.
(Well, yeah, this what locks were invented for...) To actually do this, processes just need
to access lock object by the same name.
.Pp
.Ss Functions list:
.Bl -tag -width hashdelete12345 -offset ident
.It Sy dirInitLock
Initialize a lock object for your proccess
.It Sy dirTryLock
Try to lock the lock object - give up if its already locked
.It Sy dirLock
Lock the lock object - will block until object is unlocked
.It Sy dirUnlock
Unlock the lock object
.It Sy dirDestroyLock
Destroy the lock object - free resources
.El
.Pp
Detailed interface description follows.
.\"
.\"
.Sh FUNCTIONS DESCRIPTIONS
.\"
.\" dirInitLock SUBSECTION
.Ss Cm dirInitLock Ao Ns Fa object Ns Ac Bq Aq Ns Fa spin Ns
.\"
Initialize a lock object for your process.
Only after a lock is initialized, your proccess will be able to use it.
.\"
.Sy Notice: 
This action does not lock the object. 
.Pp
The lock can be set on two types of objects. The first is an existing directory.
In this case, Aq dir must be a path (relative or full). The path must contain a 
.Ql / .
The second is an abstract object used as a lock. In this case, the name of the lock will
not contain any 
.Ql / .
This can be used to create locks without creating real directories for them.
.\"
.Sy Notice: 
Do not call your lock object 
.Ql .lock .
.Pp
Parameters:
.Bl -tag -width 1 -offset 12
.It  Aq Ns Fa object Ns Li
The name of the lock object (either existing directory or abstract name)
.It  Aq Ns Fa spin Ns Li 
The time (in seconds) that the funtion 
.Em dirLock 
will wait between two runs of 
.Em dirTryLock .
This parameter is optional, and its value generally should be less then 1.
If ommited, a default value (0.01) is set.
.El
.Pp
Return Value:
.Bd -filled -offset 12
One of the following:
.Bl -tag -width 123 -compact
.It Sy 0 
The action finished successfully.
.It Sy 1
The action failed. You do not have permissions to preform it.
.It Sy 3
The directory path could not be resolved. Possibly parameter does 
contain 
.Ql / ,
but refers to directory that does not exist.
.El
.Ed
.\"
.\"
.\" dirTryLock SUBSECTION
.Ss Cm dirTryLock Aq  Ns Fa object Ns Li
.\"
Try to lock the lock object. The function always returns
immediately.
.Pp
Parameters:
.Bl -tag -width 1 -offset 12
.It Aq Ns Fa object Ns Li 
The object that the lock is set on.
.El
.Pp
Return Value:
.Bd -filled -offset 12
One of the following:
.Bl -tag -width 123 -compact
.It Sy 0
The action finished successfully.
.It Sy 1
The action failed. The object is already locked.
.It Sy 2
The action failed. Your proccess did not initialize a lock for the object.
.It Sy 3
The directory path could not be resolved.
.El
.Ed
.\"
.\"
.\" dirLock SUBSECTION
.Ss Cm dirLock Aq Ns Fa object Ns Li
.\"
Lock given lock object.
If the object is already locked - the function will block untill the object is unlocked.
After each try (dirTryLock) the function will sleep for spin seconds (spin is defined using 
.Em dirInitLock
).
.Pp
Parameters:
.Bl -tag -width 1 -offset 12
.It Aq Ns Fa object Ns Li
The directory that the lock is set on.
.El
.Pp
Return Value:
.Bd -filled -offset 12
One of the following:
.Bl -tag -width 123 -compact
.It Sy 0
The action finished successfully.
.It Sy 2
The action failed. Your proccess did not initialize a lock for the directory.
.It Sy 3
The directory path could not be resolved.
.El
.Ed
.\"
.\"
.\" dirUnlock SUBSECTION
.Ss Cm dirUnlock Aq Ns Fa dir Ns Li
.\"
Unlock the lock object.
.Pp
Parameters:
.Bl -tag -width 1 -offset 12
.It Aq Ns Fa object Ns Li
The object that the lock is set on.
.El
.Pp
Return Value:
.Bd -filled -offset 12
One of the following:
.Bl -tag -width 123 -compact
.It Sy 0
The action finished successfully.
.It Sy 2
The action failed. Your proccess did not initialize the lock.
.It Sy 3
The directory path could not be resolved.
.El
.Ed
.\"
.\"
.\" dirDestroyLock SUBSECTION
.Ss Cm dirDestroyLock Aq Ns Fa object Ns Li
.\"
Destroys the lock object.
After this action the proccess will no longer be able to use the lock object.
To use  the object after this action is done, one must initialize the lock,
using 
.Em dirInitLock .
.Pp
Parameters:
.Bl -tag -width 1 -offset 12
.It Aq Ns Fa object Ns Li 
The directory that the lock is set on.
.El
.Pp
Return Value:
.Bd -filled -offset 12
One of the following:
.Bl -tag -width 123 -compact
.It Sy 0
The action finished successfully.
.It Sy 1
The action failed. The directory is locked by your own proccess. Unlock it first.
.It Sy 2
The action failed. Your proccess did not initialize the lock.
.It Sy 3
The directory path could not be resolved.
.El
.Ed
.\"
.\"
.Sh EXAMPLES
.\"
.\"
Creating an abstract lock named mylock, with 0.1 second spintime:
.D1 $ dirInitLock mylock 0.1 # $?=0
Locking it:
.D1 $ dirLock mylock # $?=0
Trying once to lock it again:
.D1 $ dirTryLock mylock # $?=1
Trying to lock it again:
.D1 $ dirLock mylock # Will wait forever
Unlocking:
.D1 $ dirUnlock mylock # $?=0
Destroying the lock:
.D1 $ dirDestroyLock mylock # $?=0
Trying to lock again:
.D1 $ dirLock mylock # $?=2
Creating a lock on the directory ./mydir, with default spin time:
.D1 $ dirInitLock ./mydir # $?=0
.\"
.\"
.\"
.\"
.Sh AUTHORS
.\"
.\"
.An "Hai Zaar" Aq haizaar@haizaar.com
.An "Gil Ran" Aq gil@ran4.net
.\"
.\"
.Sh SEE ALSO
.Xr ldbash 1 ,
.Xr libbash 1
