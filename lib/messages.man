.\" Process this file with
.\" groff -mdoc -Tascii <filename>
.\"
.Dd July 28, 2009
.Os Linux
.Dt MESSAGES \&3 "libbash messages Library Manual"
.\"
.Sh NAME
.\"
.\"
.Nm messages
.Nd libbash library that implements a set of functions to print standard status messages
.\"
.\"
.Sh SYNOPSIS
.\"
.Bl -tag -compact -width 1234567890
.\"
.It Cm printOK
.Op indent
.\"
.It Cm printFAIL
.Op indent
.\"
.It Cm printNA
.Op indent
.\"
.\"
.It Cm printATTN
.Op indent
.\"
.\"
.It Cm printWAIT
.Op indent
.\"
.\"
.\"
.El
.\"
.\"
.Sh DESCRIPTION
.\"
.\" GENERAL
.Ss General
.Bd -filled
.Nm
is a collection of functions to print standard status messages - those [ OK ] and
[FAIL] messages you see during Linux boot process.
.Pp
The function list:
.Bl -tag -compact -width hashdelete12345 -offset indent
.It Sy printOK
Prints a standard [ OK ] message (green)
.It Sy printFAIL
Prints a standard [FAIL] message (red)
.It Sy printNA
Prints a standard [ N/A] message (yellow)
.It Sy printATTN
Prints a standard [ATTN] message (yellow)
.It Sy printWAIT
Prints a standard [WAIT] message (yellow)
.El
.Ed
.Pp
Detailed interface description follows.
.\"
.Ss indent
Column to move to before printing.
.Pp
Default indent is calculated as TTY_WIDTH-10. If current tty width
can not be determined (for example, in case of serial console), it defaults
to 80, so default indent is 80-10=10
.\"
.Sh FUNCTIONS DESCRIPTIONS
.Ss Cm printOK Bq Ns Fa indent Ns Li 
.\"
Prints a standard [ OK ] message (green)
.\"
.\"
.Ss Cm printFAIL Bq Ns Fa indent Ns Li 
.\"
Prints a standard [FAIL] message (red)
.\"
.\"
.Ss Cm printNA Bq Ns Fa indent Ns Li 
.\"
Prints a standard [ N/A] message (yellow)
.Pp
.\"
.\"
.Ss Cm printATTN Bq Ns Fa indent Ns Li 
.\"
Prints a standard [ATTN] message (yellow)
.\"
.\"
.Ss Cm printWAIT Bq Ns Fa indent Ns Li 
.\"
Prints a standard [WAIT] message (yellow)
.\"
.\"
.Sh EXAMPLES
.\"
.\"
Run a program named MyProg, and report it's success or failure:
.Bd -literal -offset ident-two
echo -n 'Running MyProg...'
printWAIT 
if MyProg ; then
	printOK 
else
	printFAIL
fi
.Ed
.\"
.\"
.\"
.\"
.Sh AUTHORS
.\"
.\"
.An "Hai Zaar" Aq haizaar@haizaar.com
.An "Gil Ran" Aq gil@ran4.net
.\"
.\"
.Sh SEE ALSO
.Xr ldbash 1 ,
.Xr libbash 1
