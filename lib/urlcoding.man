.\" Process this file with
.\" groff -mdoc -Tascii <filename>
.\"
.Dd July 28, 2009
.Os Linux
.Dt URLCODING \&3 "libbash urlcoding Library Manual"
.\"
.Sh NAME
.\"
.\"
.Nm urlcoding
.Nd a Libbash library for encoding and decoding URL's.
.\"
.\"
.Sh SYNOPSIS
.\"
.Bl -tag -compact -width 12345678900
.\"
.It Cm urlEncodeString Bo Ns Fa -l Ns Bc Aq Ns Fa STRING Ns
.It Cm urlEncodeFile Bo Ns Fa -l Ns Bc Aq Ns Fa FILE Ns
.It Cm urlEncodeStream Bo Ns Fa -l Ns Bc
.It Cm urlDecodeString Aq Ns Fa STRING Ns
.It Cm urlDecodeFile Aq Ns Fa FILENAME Ns
.It Cm urlDecodeStream
.\"
.El
.\"
.\"
.Sh DESCRIPTION
.\"
.Bd -filled
.Nm
is a collection of functions that convert ASCII-text to standard URL's and vice-versa. The AWK code used is based on code by Heiner Steven <heiner.steven@odn.de>
.Pp
The function list:
.Bl -tag -compact -width colorprint12345 -offset indent
.It Sy urlEncodeString
Creates a URL from an ASCII string
.It Sy urlEncodeFile
Converts a file into URL-valid text
.It Sy urlEncodeStream
Converts standard input into URL-valid text
.It Sy urlDecodeString
Converts a URL-encoded text back to a plain-text form
.It Sy urlDecodeFile
Coverts URL-encoded text in a file back to plain text
.It Sy urlDecodeStream
Converts URL-encoded standard input to text
.El
.Ed
.Pp
Detailed interface description follows.
.\"
.Pp
The 
.Op Em -l
option for the encoding functions should be used when line-feed characters ('\en') are to be encoded as well.
.Pp
All functions print the results of their conversions to standard output.
.Pp
The exit status of all functions is that of the command 'awk', with '0' for success
.Pp
.\"
.Sh FUNCTIONS DESCRIPTIONS
.Pp
.Ss Cm urlEncodeString Bo Ns Fa -l Ns Bc Aq Ns Fa STRING Ns
.\"
Converts 
.Em STRING 
- a string of ASCII characters - to URL.
.Pp
.\"
.\"
.Ss Cm urlEncodeFile Bo Ns Fa -l Ns Bc Aq Ns Fa FILE Ns
.\"
Coverts 
.Em FILE 
of URL-encoded text to plain text
.\"
.Ss Cm urlEncodeStream Bo Ns Fa -l Ns Bc
.\"
Converts text from standard input to URL-text.
.Pp
.\"
.Ss Cm urlDecodeString Aq Ns Fa STRING Ns
.\"
Converts URL-encoded string
.Em STRING 
back to text.
.Pp
.\"
.Ss Cm urlDecodeFile Aq Ns Fa FILENAME Ns
.\"
Converts the URL-encoded text in 
.Em FILE
to plain text.
.Pp
.\"
.Ss Cm urlDecodeStream
.\"
Converts the URL-encoded text from standard input to plain-text
.Pp
.\"
.\"
.\"
.\"
.Sh AUTHORS
.\"
.\"
.An "Alon Keren" Aq alon.keren@gmail.com
.\"
.\"
.Sh SEE ALSO
.Xr ldbash 1 ,
.Xr libbash 1
