.\" Process this file with
.\" groff -mdoc -Tascii <filename>
.\"
.Dd July 28, 2009
.Os Linux
.Dt HASHSTASH \&3 "libbash hashstash Library Manual"
.\"
.Sh NAME
.\"
.\"
.Nm hashstash
.Nd libbash library that implements hash data structure
.\"
.\"
.Sh SYNOPSIS
.\"
.Bl -tag -compact -width 12345678901234567
.\"
.It Cm hashSet
.Aq Value
.Aq Key
.Aq HashName
.Op SubHashName Op ...
.\"
.It Ft $retval Cm hashGet
.Aq Key
.Aq HashName
.Op SubHashName Op ...
.\"
.It Ft $retval Cm hashKeys
.Aq HashName
.Op SubHashName Op ...
.\"
.It Cm hashRemove
.Aq Key
.Aq HashName
.Op SubHashName Op ...
.\"
.It Cm hashDelete
.Aq HashName
.Op SubHashName Op ...
.\"
.\"
.El
.\"
.\"
.Sh DESCRIPTION
.\"
.\" GENERAL
.Ss General
.Bd -filled
.Nm
is a collection of functions that implement basic hash data-structure in bash scripting language.
.Pp
The function list:
.Bl -tag -compact -width hashdelete12345 -offset indent
.It Sy hashSet
Adds a value to the hash
.It Sy hashGet
Returns a value from the hash
.It Sy hashKeys
Returns a list of keys of the hash
.It Sy hashRemove
Removes a key from the hash
.It Sy hashDelete
Deletes a hash
.El
.Ed
.Pp
Detailed interface description follows.
.\"
.Sh FUNCTIONS DESCRIPTIONS
.\" hashSet SUBSECTION
.Ss Cm hashSet Ao Ns Fa Value Ns Ac Ao Ns Fa Key Ns Ac Ao Ns Fa Hashname Ns Ac Op SubHashName Op ...
.\"
Adds a value to the hash.
.Pp
Parameters:
.Bl -tag -width 1 -offset 12
.It  Aq Ns Fa Value Ns 
The value to set in 
.Fa HashName Ns Bo Fa Key Bc .
.It Aq Ns Fa Key Ns 
The key for the value 
.Fa Value .
.It Ao Ns Fa HashName Ns Ac Op SubHashName Op ...
A string that contains the name of the hash.
If the hash is a sub hash of another hash, the "father hash" name MUST BE WRITTEN FIRST, followed by the sub-hash name.
.El
.Pp
.Fa Value 
will be the value of the key
.Fa Key
in the hash 
.Fa HashName .
For example if you have (or want to define) hash 
.Em C ,
which is subhash of hash
.Em B ,
which is subhash of hash
.Em A ,
and 
.Em C
has a key named
.Em ckey1
with value 
.Em cval1 ,
then you should use:
.D1 Sy hashSet cval1 ckey1 A B C
.\"
.\" hashGet SUBSECTION
.Ss Ft $retval Cm hashGet Ao Ns Fa Key Ns Ac Ao Ns Fa HashName Ns Ac Op SubHashName Op ...
.\"
Returns the value of 
.Fa Key 
in 
.Fa HashName 
to the 
.Ft $retval 
variable.
.Pp
Parameters:
.Bl -tag -width 1 -offset 12
.It Aq Ns Fa Key Ns
The key that hold the value we wish to get.
.It Ao Ns Fa HashName Ns Ac Op SubHashName Op ...
A string that contains the name of the hash.
If the hash is a sub hash of another hash, the 
.Qq father hash 
name MUST BE WRITTEN FIRST, followed by the sub-hash name.
.El
.Pp
Return Value:
.Bd -filled -offset 12 -compact
The value of the key
.Fa Key
in the hash 
.Fa HashName . 
The value is returned in the variable 
.Ft $retval .
.Ed
.\"
.\" hashKeys SUBSECTION
.Ss Ft $retval Cm hashKeys Ao Ns Fa HashName Ns Ac Op SubHashName Op ...
.\"
Returns a list of keys of the hash 
.Fa HashName
in the variable
.Ft $retval .
.Pp
Parameters:
.Bl -tag -width 1 -offset 12
.It Ao Ns Fa HashName Ns Ac Op SubHashName Op ...
A string that contains the name of the hash.
If the hash is a sub hash of another hash, the 
.Qq father hash 
name MUST BE WRITTEN FIRST, followed by the sub-hash name.
.El
.Pp
Return Value:
.Bd -filled -offset 12
The value of the key
.Fa Key
in the hash 
.Fa HashName . 
The value is returned in the variable 
.Ft $retval .
.Ed
.\"
.\" hashRemove SUBSECTION
.Ss Cm hashRemove Ao Ns Fa Key Ns Ac Ao Ns Fa HashName Ns Ac Op SubHashName Op ...
.\"
Removes the key 
.Fa Key
from the hash
.Fa HashName .
.Bl -tag -width 1 -offset 12
.It Aq Ns Fa Key Ns 
The key we wish to remove from 
.Fa HashName .
.It Ao Ns Fa HashName Ns Ac Op SubHashName Op ...
A string that contains the name of the hash.
If the hash is a sub hash of another hash, the 
.Qq father hash
name MUST BE WRITTEN FIRST, followed by the sub-hash name.
.El
.Pp
This function should also be used to remove a sub-hash from its 
.Qq father hash . 
In that case, the
.Fa key 
will be the name of the sub-hash.
.\"
.\" hashDelete SUBSECTION
.Ss Cm hashDelete Ao Ns Fa HashName Ns Ac Op SubHashName Op ...
.\"
Deletes the hash 
.Fa HashName Op SubHashName Op ... .
.Pp
Parameters:
.Bl -tag -width 1 -offset 12
.It Ao Ns Fa HashName Ns Ac Op SubHashName Op ...
A string that contains the name of the hash.
If the hash is a sub hash of another hash, the 
.Qq father hash
name MUST BE WRITTEN FIRST, followed by the sub-hash name.
.El
.Pp
If this function is used on a sub-hash, a key with the name of the sub-hash will remain in its 
.Qq father hash
and will hold a
NULL value.
.\"
.\"
.\"
.\"
.Sh BUGS
.\"
.\"
.Bd -filled
.Pp
A hash name can only contain characters that are valid as part of bash variable names (i.e. a-zA-Z0-9_).
The same applies for hash keys.
.Pp
As for now, there is no way of knowing if a key represents a value or a sub-hash.
If a sub-hash will be used as a key, the returned value will be its keys list.
.Ed
.\"
.\"
.Sh EXAMPLES
.\"
.\"
Define hash table 
.Em hashA
with key
.Em Akey1
with value
.Em Aval1
use:
.Dl Sy % hashSet Aval1 Akey1 Ahash
Now:
.Dl Sy % hashGet Akey1 Ahash
.Dl Sy % echo $retval
.Dl Sy Aval1
.Dl Sy % hashKeys Ahash
.Dl Sy % echo $retval
.Dl Sy Akey1
.Dl Sy %
.\"
.\"
.Sh HISTORY
.\"
.\"
.Bd -filled
The idea to write 
.Nm
library appeared when we've discovered the full power of the bash 
.Em eval
function.
.Pp
As of the name
.Nm ,
it has two meanings. The first, it means 
.Ql stash
of hash functions. The second is, that 
.Nm
contains subhashes inside, so it looks like stash of packed information.
.Ed
.\"
.\"
.Sh AUTHORS
.\"
.\"
.An "Hai Zaar" Aq haizaar@haizaar.com
.An "Gil Ran" Aq gil@ran4.net
.\"
.\"
.Sh SEE ALSO
.Xr ldbash 1 ,
.Xr libbash 1
