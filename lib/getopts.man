.\" Process this file with
.\" groff -mdoc -Tascii <filename>
.\"
.Dd July 28, 2009
.Os Linux
.Dt GETOPTS \&3 "libbash getopts Library Manual"
.\"
.Sh NAME
.\"
.\"
.Nm getopts
.Nd libbash library for command line parameters parsing
.\"
.\"
.Sh SYNOPSIS
.\"
.\"
.Ft $retval
.Cm getopt_long
.Aq Instructions
.Aq Parameters
.\"
.\"
.Sh DESCRIPTION
.\"
.\"
.Bd -filled
This is a documentation for
.Em libbash
getopts library, that implements
.Em getopt_long
function for
.Xr bash 1 .
For documentation of bash getopts function, please see
.Xr getopts 1
(
.Xr getopts 1posix
on some systems).
.Pp
Here is a table for reference:
.Bl -tag -compact -width 1234567891234567
.It Xr getopts 1
(or 1posix on some systems) implemented by
.Em bash
.It Xr getopts 3
implemented by
.Em libbash .
.It Xr getopt 1
implemented by getopt utils (part of util-linux)
.It Xr getopt_long 1
implemented by
.Em libbash
and installed to section 1 instead of 3 to prevent collision with C man pages.
.It Xr getopt 3
implemented by GNU C library.
.It Xr getopt_long 3
implemented by GNU C library.
.El
I have also seen separate getopt utility which part of util-linux package.
.Pp
The 
.Em getopt_long 
function parses the command line arguments.  It uses
.Fa Instructions
as the rules for parsing the
.Fa Parameters .
.Ed
.\"
.Ss The Instructions
A string that specifies rules for parameters parsing.
The instructions string is built of a group of independent instructions, separated by a white space.
Each instruction must have the following structure:
.Pp
.Sy -<SingleLetter>|--<MultiLetter>-><VariableName>[:]
.Pp
This structure contains three parts:
.Bl -tag -width 12345
.It Sy -<SingleLetter>
This is the parameter single-letter sign. For example 
.Fl h .
.Pp
.It Sy --<MultiLetter>
This is the parameter's corresponding multi-letter sign. For example 
.Fl -help .
.Pp
.It Sy <VariableName>[:]
This is the name of the variable that will contain the parameter value. For example:
.Sy HELP .
.Pp
The Variable name can represent one of two variables types:
.Bl -tag -width 123
.It Sy Flag variable Li (not followed by Ql \&: )
In this case, it will hold the value 1 if 
.Ql on
(i.e. was specified on command line) and will not be defined if 
.Ql off .
.It Sy Value variable Li (followed by Ql \&: ) 
In this case, the value it will hold is the string that was given as the next parameter in the
.Fa Parameters
string (Separated by white-space or 
.Ql =
). If input contains more then one instance of the considered command line option, an array of 
the given parameters will be set as the value of the variable.
.El
.El
.\"
.Ss The Parameters
The 
.Fa Parameters 
are simply the parameters you wish to parse.
.\"
.\"
.Sh RETURN VALUE
.\"
.\"
This function returns a string that contains a set of variables definitions.
In order to define the variables, this string should be given as a parameter to
.Em eval 
function. This value is returned in the variable 
.Fa $retval .
.\"
.\"
.Sh EXAMPLES
.\"
.\"
Parse command line parameters looking for the flags 
.Fl h | Fl -help 
and
.Fl v | Fl -version
and for the value
.Fl p | Fl -path
:
.Bd -literal -offset indent
getopt_long '\-h|\-\-help\->HELP 
              \-v|\-\-version\->VERSION 
              \-p|\-\-path\->PATH:' $*
eval $retval
.Ed
.Pp
In this example, for the parameters
.Sy --help --path=/usr/
the variables that will be created are:
.Bd -literal -offset indent
HELP=1
PATH=/usr/
.Ed
.\"
.Pp
for the parameters 
.Sy --help --path=/usr --path=/bin
the variables that will be created are:
.Bd -literal -offset indent
HELP=1
PATH=(/usr /bin)
.Ed
.\"
.\"
.Sh BUGS
.\"
.\"
.Bd -filled
.Pp
Values must not contain the string `__getopts__'. This string will be parsed as a single
white-space.
.Pp
A value should not start with an already defined multi-letter sign. If such a value exists,
it will be treated as the equivalent singe-letter sign. This bug only accures when using a
single-letter sign, or a multi-letter sign that are not followed by a `='.

For example:
If we have a script named `foo', and we parse the parameters `\-d|\-\-dir:' and `\-f|\-\-file:', then
.Bd -literal -offset indent
foo \-d \-\-file
.Ed
and
.Bd -literal -offset indent
foo \-\-dir \-\-file 
.Ed
will not work
.Bd -literal -offset indent
foo \-\-dir=\-\-file
.Ed
will work.

.Ed
.\"
.\"
.Sh AUTHORS
.\"
.\"
.An "Hai Zaar" Aq haizaar@haizaar.com
.An "Gil Ran" Aq gil@ran4.net
.\"
.\"
.Sh SEE ALSO
.Xr ldbash 1 ,
.Xr getopt_long 1 ,
.Xr getopts 1 ,
.Xr getopt 1 ,
.Xr libbash 1 ,
.Xr getopt 3 ,
.Xr getopt_long 3
.\"
.\"
