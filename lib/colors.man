.\" Process this file with
.\" groff -mdoc -Tascii <filename>
.\"
.Dd July 28, 2009
.Os Linux
.Dt COLORS \&3 "libbash colors Library Manual"
.\"
.Sh NAME
.\"
.\"
.Nm colors
.Nd libbash library for setting tty colors.
.\"
.\"
.Sh SYNOPSIS
.\"
.Bl -tag -compact -width 12345678900
.\"
.It Cm colorSet
.Aq Ar color
.\"
.It Cm colorReset
.\"
.It Cm colorPrint
.Op Aq indent
.Aq Ar color
.Aq Ar text
.\"
.It Cm colorPrintN
.Op Aq indent
.Aq Ar color
.Aq Ar text
.\"
.\"
.El
.\"
.\"
.Sh DESCRIPTION
.\"
.\" GENERAL
.Ss General
.Bd -filled
.Nm
is a collection of functions that make it very easy to put colored text on tty.
.Pp
The function list:
.Bl -tag -compact -width colorprint12345 -offset indent
.It Sy colorSet
Sets the color of the prints to the tty to COLOR
.It Sy colorReset
Resets current tty color back to normal
.It Sy colorPrint
Prints TEXT in the color COLOR indented by INDENT (without adding a newline)
.It Sy colorPrintN
The same as colorPrint, but trailing newline is added
.El
.Ed
.Pp
Detailed interface description follows.
.\"
.Pp
.Ss Available colors:
.Bl -tag -compact -width hashdelete12345 -offset indent
.It Sy Green
.It Sy Red
.It Sy Yellow
.It Sy White
.El
The color parameter is non-case-sensitive (i.e. RED, red, ReD, and all the other forms are valid and are the
same as Red).
.\"
.Sh FUNCTIONS DESCRIPTIONS
.Ss Cm colorSet Aq Ns Fa color Ns
.\"
Sets the current printing color to 
.Em color .
.Pp
.\"
.\"
.Ss Cm colorReset 
.\"
Resets current tty color back to normal.
.\"
.\"
.Ss Cm colorPrint Bo Ao Ns Fa indent Ns Ac Bc Aq Ns Fa color Ns
.\"
Prints 
.Em text 
using the color 
.Em color 
indented by 
.Em indent
(without adding a newline).
.Pp
Parameters:
.Bl -tag -width 1 -offset 12
.It  Aq Ns Fa indent Ns Li 
The column to move to before start printing.
This parameter is optional. If ommitted - start output from current cursor position.
.It Aq Ns Fa color Ns Li
The color to use.
.It Aq Ns Fa color Ns Li
The text to print.
.El
.Pp
.\"
.\"
.Ss Cm colorPrintN Bo Ao Ns Fa indent Ns Ac Bc Aq Ns Fa color Ns
The same as colorPrint, except a trailing newline is added.
.\"
.\"
.\"
.\"
.Sh EXAMPLES
.\"
.\"
Printing a green 'Hello World' with a newline:
.Bl -tag -width 1 -offset 12345
.It Using colorSet:
.D1 $ colorSet green
.D1 $ echo 'Hello World'
.D1 $ colorReset
.It Using colorPrint:
.D1 $ colorPrint 'Hello World' ; echo
.It Using colorPrintN:
.D1 $ colorPrintN 'Hello World'
.El
.\"
.\"
.\"
.\"
.Sh AUTHORS
.\"
.\"
.An "Hai Zaar" Aq haizaar@haizaar.com
.An "Gil Ran" Aq gil@ran4.net
.\"
.\"
.Sh SEE ALSO
.Xr ldbash 1 ,
.Xr libbash 1
