.\" Process this file with
.\" groff -mdoc -Tascii <filename>
.\"
.Dd July 28, 2009
.Os Linux
.Dt LIBBASH \&7 "libbash Manual"
.\"
.Sh NAME
.\"
.\"
.Nm libbash
.Nd A bash shared libraries package.
.\"
.\"
.Sh DESCRIPTION
.\"
.\"
.Bd -filled
.Nm
is a package that enables bash dynamic-like shared libraries. Actually its a tool for managing
bash scripts whose functions you may want to load and use in scripts of your own.
.Pp
It contains a 
.Ql dynamic loader 
for the shared libraries (
.Ns Xr ldbash 1 Ns ), a configuration tool ( Ns Xr ldbashconfig 8 Ns ), and some libraries.
.Pp
Using
.Xr ldbash 1 you are able to load loadable bash libraries, such as Xr getopts 1 and Xr hashstash 1 Ns .
A bash shared library that can be loaded using 
.Ed
.Xr ldbash 1 must answer 4 requirments:
.Bl -enum -width 12345
.It 
It must be installed in 
.Fa $LIBBASH_PREFIX Ns /lib/bash (default is /usr/lib/bash).
.It
It must contain a line that begins with 
.Ql #EXPORT= .
That line will contain (after the 
.Ql = Ns
) a list of functions that the library exports.
I.e. all the function that will be usable after loading that library will be listed in that line.
.It 
It must contain a line that begins with 
.Ql #REQUIRE= .
That line will contain (after the 
.Ql = Ns
) a list of bash libraries that are required for our library.
I.e. every bash library that is in use in our bash library must be listed there.
.It 
The library must be listed (For more information, see 
.Xr ldbashconfig 8 Ns ). 
.El
.Pp
.Ss Basic guidelines for writing library of your own:
.Bl -enum -width 12345
.It 
Be aware, that your library will be actually sourced. So, basically, it should contain (i.e define) only
functions.
.It 
Try to declare all variables intended for internal use as local.
.It 
Global variables and functions that are intended for internal use (i.e are not defined in
.Ql #EXPORT= )
should begin with:
.Dl Sy __<library_name>_
For example, internal function 
.Em myfoosort
of 
.Em hashstash
library should be named as
.Dl Sy __hashstash_myfoosort
This helps to avoid conflicts in global name space when using libraries that come from different
vendors.
.It
See html manual for full version of this guide.
.El
.\"
.\"
.Sh AUTHORS
.\"
.\"
.An "Hai Zaar" Aq haizaar@haizaar.com
.An "Gil Ran" Aq ril@ran4.net
.\"
.\"
.Sh SEE ALSO
.\"
.\"
.Xr ldbash 1 ,
.Xr ldbashconfig 8 ,
.Xr getopts 1 ,
.Xr hashstash 1
.Xr colors 1
.Xr messages 1
.Xr urlcoding 1
.Xr locks 1
